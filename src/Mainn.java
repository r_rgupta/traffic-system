import java.util.Scanner;


// Assumptions
// Every vehicle is engine powered (means uses fuel like Diesel Petrol CNG)
// Every vehicle is registered.
// There are two types of Vehicle, Personal & Commercial ( passenger vehicle )
// Every passenger vehicle has permit.

public class Mainn {

	public static void main(String[] args) {
		
		System.out.println("Welcome to the Bangalore traffic System , Happy Journey !!");
		System.out.println();
		while(true)
		{
			System.out.print("Add Your Vehicle. Press 0 for Personal vehicle, 1 for Passenger vehicle : ");
		    Scanner sc = new Scanner(System.in);
		    byte input = sc.nextByte();
		    if(input==0)
		    {
		    	// isFourWheeler
		    	boolean input1;
			    while(true)
			    {
			        System.out.print("Press 0 for TwoWheeler, 1 for FourWheeler : ");
			        byte input1_t = sc.nextByte();
			        if(input1_t==0)
			        {
			        	input1 = false;
			        	break;
			        }
			        else if(input1_t==1)
			        {
			        	input1 = true;
			        	break;
			        }
			        else
			        {
			        	System.out.println("Invalid Input !!! Enter Correct Value..... ");
			        }
			    }
			    
			    // Total number of people using it at this time
			    System.out.print("Enter the total number of people using it at this time : ");
			    byte input2 = sc.nextByte();
			    
			    // Fuel type
			    System.out.print("Enter the Fuel type : ");
			    String input3 = sc.next();
			    
			   // regNo
			    System.out.print("Enter Registration No : ");
		    	int input4 = sc.nextInt();
		    	
		    	// regValidity
		    	System.out.print("Enter the Validity Date of Registration. i.e. DD of DD/MM/YYYY : ");
		    	int input5 = sc.nextInt();
		    	System.out.print("Enter the Validity Month of Registration. i.e. MM of DD/MM/YYYY : ");
		    	int input6 = sc.nextInt();
		    	System.out.print("Enter the Validity Year of Registration. i.e. YYYY of DD/MM/YYYY : ");
		    	int input7 = sc.nextInt();
		    	
		    	// fineAmount
		    	System.out.print("Enter the current fine Amount : ");
		    	int input8 = sc.nextInt();
		    	
		    	// Constructing Vehicle
			    Vehicle personalVehicle = new Vehicle(input1,input2,input3,input4,input5,input6,input7,input8);
			    personalVehicle.PUC_Check();
			    personalVehicle.RTO_Check_PVC();
			    personalVehicle.show();
			    
			    System.out.println();
			    }
		    else if(input==1)
		    {
		    	// Total number of people using it at this time
		        System.out.print("Enter the total number of people using it at this time : ");
		        byte input2 = sc.nextByte();
		    
		        // Fuel type
		        System.out.print("Enter the Fuel type : ");
		        String input3 = sc.next();
		    
		        // regNo
		        System.out.print("Enter Registration No : ");
	    	    int input4 = sc.nextInt();
	    	
	    	    // regValidity
	    	    System.out.print("Enter the Validity Date of Registration. i.e. DD of DD/MM/YYYY : ");
	    	    int input5 = sc.nextInt();
	    	    System.out.print("Enter the Validity Month of Registration. i.e. MM of DD/MM/YYYY : ");
	    	    int input6 = sc.nextInt();
	    	    System.out.print("Enter the Validity Year of Registration. i.e. YYYY of DD/MM/YYYY : ");
	    	    int input7 = sc.nextInt();
	    	
	    	    // fineAmount
	    	    System.out.print("Enter the current fine Amount : ");
	    	    int input8 = sc.nextInt();
	    	
	    	    // designedCapacity
	    	    System.out.print("Enter the designed Capacity : ");
	    	    byte input9 = sc.nextByte();
	    	
	    	    // no of staff required to operate
	    	    byte input10;
	    	    while(true)
	    		{
	    	    	System.out.print("No of Staff required to operate : ");
	    		    input10 = sc.nextByte();
	    		    if(input10>input9)
	    		    {
	    		    	System.out.println("Number of staff required to operate the vehicle cannot be greater than the designed capacity !!!");
	    		    }
	    		    else
	    		    {break;}
	    		}
	    		
	    	    // Permit Validity
	    	    System.out.print("Enter the Permit No : ");
	    	    int input11 = sc.nextInt();
	    	
	    	    // PermitValidity
	    	    System.out.print("Enter the Validity Date of Permit of Passenger Vehicle. i.e. DD of DD/MM/YYYY : ");
	    	    int input12 = sc.nextInt();
	    	    System.out.print("Enter the Validity Month of Permit of Passenger Vehicle. i.e. MM of DD/MM/YYYY : ");
	    	    int input13 = sc.nextInt();
	    	    System.out.print("Enter the Validity Year of Permit of Passenger Vehicle. i.e. YYYY of DD/MM/YYYY : ");
	    	    int input14 = sc.nextInt();
	    	
	    	    // approved Capacity
	    	    System.out.print("Enter the approved paid passenger capacity : ");
	    	    int input15 = sc.nextInt();
	    	    
	    	    // Constructing Vehicle
	    	    PassengerVehicle passengerVehicle = new PassengerVehicle(input2,input3,input4,input5,input6,input7,input8,input9,input10,input11,input12,input13,input14,input15);
		        passengerVehicle.PUC_Check();
		        passengerVehicle.RTO_Check_PVC();
		        passengerVehicle.show();
		        
		        System.out.println();
		        }
		    else
		    {
		    	System.out.println("Invalid Input !!! Enter Correct Value..... ");
		    }
		    }
		}
}
