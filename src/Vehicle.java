import java.util.Scanner;
import java.util.Formatter;  
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.time.LocalDateTime;  
import java.util.Random;  
import java.util.Date;  
import java.time.*;

public class Vehicle {
	
	public Vehicle(boolean a,byte b,String c,int d,int e,int f,int g,int h) {
		
		isFourWheeler = a;
		noOfPeopleUsingVehicleAtThisTime = b;
		fuelType = c;
		regNo =d;
		regValidity = LocalDate.of(g, f, e);
		
		Random random = new Random(); 
		emissionLevel = 1+random.nextInt(9);
		fineAmount = h;
		
		System.out.println("Vehicle Successfully added.");
	}
	
	public boolean isFourWheeler() {
		return isFourWheeler;
	}

	public byte getNoOfPeopleUsingVehicleAtThisTime() {
		return noOfPeopleUsingVehicleAtThisTime;
	}

	public String getFuelType() {
		return fuelType;
	}

	public int getEmissionLevel() {
		return emissionLevel;
	}

	public int getRegNo() {
		return regNo;
	}

	public LocalDate getRegValidity() {
		return regValidity;
	}

	public int getFineAmount() {
		return fineAmount;
	}

	private boolean isFourWheeler;
	private byte noOfPeopleUsingVehicleAtThisTime;
	private String fuelType;
	private int emissionLevel;
	private int regNo;
	private LocalDate regValidity;
	private int fineAmount;
	
	public void PUC_Check() {
		Random random = new Random(); 
		emissionLevel = 1+random.nextInt(9);
	}
	
	public void RTO_Check() {
		
		LocalDate d1 = LocalDate.now();
		
		if(d1.isAfter(regValidity))
		{
			fineAmount += 2000;
		}
		if(emissionLevel>7)
		{
			fineAmount += 500;
		}
	}
	
	public void show() {
		
		Formatter fmt = new Formatter();
		fmt.format("%40s %20s\n", "isFourWheeler", isFourWheeler);
		fmt.format("%40s %20s\n", "noOfPeopleUsingVehicleAtThisTime", noOfPeopleUsingVehicleAtThisTime);
		fmt.format("%40s %20s\n", "fuelType", fuelType);
		fmt.format("%40s %20s\n", "emissionLevel", emissionLevel);
		fmt.format("%40s %20s\n", "regNo", regNo);
		fmt.format("%40s %20s\n", "regValidity", regValidity);
		fmt.format("%40s %20s\n", "fineAmount", fineAmount);
		System.out.print(fmt);
	}
	
}
