import java.time.LocalDate;
import java.util.Formatter;
import java.util.Scanner;

public class PassengerVehicle extends Vehicle{
	public PassengerVehicle(byte a,String b,int c,int d,int e,int f,int g,byte h,byte i,int j,int k,int l,int m,int n)
	{
		super(true,a,b,c,d,e,f,g);
		designedCapacity = h;
		staff = i;
		permitNo = j;
		permitValidity = LocalDate.of(m,l, k);
		approvedPaidPassengerCapacity = n;
	}
	private byte designedCapacity;
	private byte staff;
	private int permitNo;
	private LocalDate permitValidity;
	private int approvedPaidPassengerCapacity;
	
	@Override
	public void show() {
		Formatter fmt = new Formatter();
		fmt.format("%40s %20s\n", "isFourWheeler", isFourWheeler());
		fmt.format("%40s %20s\n", "noOfPeopleUsingVehicleAtThisTime", getNoOfPeopleUsingVehicleAtThisTime());
		fmt.format("%40s %20s\n", "fuelType", getFuelType());
		fmt.format("%40s %20s\n", "emissionLevel", getEmissionLevel());
		fmt.format("%40s %20s\n", "regNo", getRegNo());
		fmt.format("%40s %20s\n", "regValidity", getRegValidity());
		fmt.format("%40s %20s\n", "fineAmount", getFineAmount());
		fmt.format("%40s %20s\n", "designedCapacity", getDesignedCapacity());
		fmt.format("%40s %20s\n", "staff", getStaff());
		fmt.format("%40s %20s\n", "permitNo", getPermitNo());
		fmt.format("%40s %20s\n", "permitValidity", getPermitValidity());
		fmt.format("%40s %20s\n", "approvedPaidPassengerCapacity", getApprovedPaidPassengerCapacity());
		System.out.print(fmt);
	}

	public byte getDesignedCapacity() {
		return designedCapacity;
	}

	public byte getStaff() {
		return staff;
	}

	public int getPermitNo() {
		return permitNo;
	}

	public LocalDate getPermitValidity() {
		return permitValidity;
	}

	public int getApprovedPaidPassengerCapacity() {
		return approvedPaidPassengerCapacity;
	}
	
	@Override
    public void RTO_Check() {
		
		LocalDate d1 = LocalDate.now();
		
		if(d1.isAfter(regValidity()))
		{
			fineAmount += 2000;
		}
		if(d1.isAfter(permitValidity))
		{
			fineAmount += 3000;
		}
		if(emissionLevel>7)
		{
			fineAmount += 500;
		}
	}
}
